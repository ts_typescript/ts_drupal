declare var jQuery:any;
declare var Drupal:any;
declare var drupalSettings:any;

export var $ = jQuery;

export var D = {
    drupal: Drupal,
    settings: drupalSettings,
    isDev: function(){
        return D.settings.env === 'development';
    },
    addBehavior: function(id, object){
        Drupal.behaviors[id] = object;
    }
};

export var T = {
    /**
     * Retourn vrai si l'élément est une fonction.
     *
     * @returns {boolean}
     */
    isFunction: function(arg){
        return typeof( arg ) === 'function';
    },

    /**
     * Retourn vrai si l'élément est défini.
     *
     * @returns {boolean}
     */
    isDefined: function(arg){
        return typeof( arg ) !== 'undefined'
            && arg != null
            && !isNaN(arg);
    },

    /**
     * Renvoie vrai si le context est root (document)
     * @param context
     */
    contextIsRoot: function( context){
        return 'HTML' === $($(context).children()[0]).prop("tagName");
    },

    /**
     * Renvoie vrai si on est en contexte d'administration.
     */
    isAdminContext: function(){
        return $('#toolbar-administration').length > 0;
    },
};

/**
 * Redéfinition de l'erreur
 **/
D.drupal.throwError = function (error) {
    setTimeout(function () {
        if( D.isDev() ){
            console.log(error);
        }
    }, 0);
};