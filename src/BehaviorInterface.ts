export interface BehaviorInterface{
    attach(context:any);
    detach(context:any);
}